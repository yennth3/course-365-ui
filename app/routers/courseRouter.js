const express = require("express");

const courseRouter = express.Router();
const {getAllCourses, createCourse}=require("../controllers/courseController");

courseRouter.get("/course365/courses", getAllCourses);
courseRouter.post("/course365/courses", createCourse);
module.exports = courseRouter;