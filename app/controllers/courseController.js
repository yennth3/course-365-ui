const mongoose = require("mongoose");
const courseModel = require("../models/courseModel");

const getAllCourses = (req, res)=>{
    courseModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                message:`Internal server error : ${err.message}`
            })
        }
        else{
            return res.status(200).json({
                courses:data
            })
        }
    })
}
//create a Course
const createCourse = (request, response) => {
    //b1: thu thập dữ liệu
    let body = request.body;
    //b2: kiểm tra dữ liệu
    if (!body.courseCode) {
        return response.status(400).json({
            message: "courseCode is invalid"
        })
    }
    if (!body.courseName) {
        return response.status(400).json({
            message: "courseName is invalid"
        })
    }
    if (!body.price) {
        return response.status(400).json({
            message: "price is invalid"
        })
    }
    if (!body.discountPrice) {
        return response.status(400).json({
            message: "discountPrice is invalid"
        })
    }
    if (!body.duration) {
        return response.status(400).json({
            message: "duration is invalid"
        })
    }
    if (!body.level) {
        return response.status(400).json({
            message: "level is invalid"
        })
    }
    if (!body.coverImage) {
        return response.status(400).json({
            message: "coverImage is invalid"
        })
    }
    if (!body.teacherName) {
        return response.status(400).json({
            message: "teacherName is invalid"
        })
    }
    if (!body.teacherPhoto) {
        return response.status(400).json({
            message: "teacherPhoto is invalid"
        })
    }
 
    //b3: thực hiện
    let course = {
        _id: mongoose.Types.ObjectId(),
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular:body.isPopular,
        isTrending:body.isTrending

    }
    courseModel.create(course, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        } else {
            return response.status(201).json({
                courses:data
            })
        }
    });



};
module.exports = {getAllCourses, createCourse}