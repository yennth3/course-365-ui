//b1: khai báo thư viện mongoose
const mongoose = require ('mongoose');
//b2: khai báo thư viện Schema
const Schema = mongoose.Schema;

//b3: tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const courseSchema = new Schema({
    _id:{
        type: mongoose.Types.ObjectId
     },
    courseCode:{
        type: String,
        required:true,
        unique:true
    },
    courseName:{
        type: String,
        required:true
    },
    price:{
        type: Number,
        required:true
    },
    discountPrice:{
        type: Number,
        required:true
    },
    duration:{
        type: String,
        required:true
    },
    level:{
        type: String,
        required:true
    },
    coverImage:{
        type: String,
        required:true
    },
    teacherName:{
        type: String,
        required:true
    },
    teacherPhoto:{
        type: String,
        required:true
    },
    isPopular:{
        type: Boolean,
        default:true
    },
    isTrending:{
        type: Boolean,
        default:false
    },
    ngayTao:{
        type: Date,
        default:Date.now()
    },
    ngayCapNhat:{
        type: Date,
        default:Date.now()
    }
})

//b4: Export ra 1 model cho schema
module.exports = mongoose.model("course", courseSchema);