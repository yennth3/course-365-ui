//khai báo thư viện express
const express = require ('express');
const path = require('path');
const app = new express();
const mongoose = require('mongoose');
const port = 8000;

// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))
//import model
const courseModel = require('./app/models/courseModel');
//import router
const courseRouter = require('./app/routers/courseRouter');

app.use(express.static('views'));

app.get("/", (request, response)=>{
    response.sendFile(path.join(__dirname+'/views/index.html'));
})
//kết nối csdl mongodb
mongoose.connect('mongodb://localhost:27017/Course365',(error)=>{
    if(error){
        throw error;
    }
    console.log("successfully connected!")
})

app.use('/', courseRouter);
app.listen(port, () =>{
    console.log(`App chạy trên cổng ${port} `)
})